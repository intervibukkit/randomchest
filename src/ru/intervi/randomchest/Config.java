package ru.intervi.randomchest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class Config {
	public Config(Main main) {
		this.main = main;
	}
	
	private Main main;
	
	private HashSet<String> chests = new HashSet<String>();
	private HashMap<ItemStack, Float> loot = new HashMap<ItemStack, Float>();
	
	public ItemStack key = null;
	public float keyRandom = 0.5F;
	public int blocks = 1000;
	public int slots = 27;
	public ItemStack fillItem = null;
	public ItemStack frameItem = null;
	public int maxMoney = 9;
	public int itemAmount = 2;
	
	public int saveSec = 60;
	
	public String moneyMsg = "&eвам выдано %money%";
	public String inventory = "Бонусный сундук";
	public String keyGive = "&eвыпал ключ";
	
	public boolean hasChest(Location loc) {
		return chests.contains(getStrLoc(loc));
	}
	
	public Set<Entry<ItemStack, Float>> getLoot() {
		return loot.entrySet();
	}
	
	public static String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	public static List<String> color(List<String> list) {
		ArrayList<String> result = new ArrayList<String>();
		for (String s : list) result.add(color(s));
		return result;
	}
	
	public static String getStrLoc(Location loc) {
		return loc.getWorld().getName() + ',' + loc.getBlockX() + ',' + loc.getBlockY() + ',' + loc.getBlockZ();
	}
	
	public void addChestLocation(Location loc) {
		String strLoc = getStrLoc(loc);
		if (chests.contains(strLoc)) return;
		chests.add(strLoc);
		FileConfiguration conf = main.getConfig();
		ArrayList<String> chestsList = conf.isSet("chests") ? new ArrayList<String>(conf.getStringList("chests")) : new ArrayList<String>();
		chestsList.add(strLoc);
		conf.set("chests", chestsList);
		main.saveConfig();
	}
	
	public boolean removeChestLocation(Location loc) {
		String strLoc = getStrLoc(loc);
		if (!chests.contains(strLoc)) return false;
		chests.remove(strLoc);
		ArrayList<String> chestsList = new ArrayList<String>();
		for (String str : chests) chestsList.add(str);
		main.getConfig().set("chests", chestsList);
		main.saveConfig();
		return true;
	}
	
	@SuppressWarnings("deprecation")
	public void load() {
		main.saveDefaultConfig();
		main.reloadConfig();
		FileConfiguration conf = main.getConfig();
		keyRandom = (float) conf.getDouble("key-random");
		blocks = conf.getInt("blocks");
		slots = conf.getInt("slots");
		maxMoney = conf.getInt("max-money");
		itemAmount = conf.getInt("item-amount");
		saveSec = conf.getInt("save-sec");
		moneyMsg = color(conf.getString("money-msg"));
		inventory = color(conf.getString("inventory"));
		keyGive = color(conf.getString("key-give"));
		
		byte fillColor = (byte) conf.getInt("fill-color");
		if (fillColor != -1) fillItem = new ItemStack(Material.valueOf(conf.getString("fill-item")), 1, (short) 0, fillColor);
		else fillItem = new ItemStack(Material.valueOf(conf.getString("fill-item")));
		byte frameColor = (byte) conf.getInt("frame-color");
		if (frameColor != -1) frameItem = new ItemStack(Material.valueOf(conf.getString("frame-item")), 1, (short) 0, frameColor);
		else frameItem = new ItemStack(Material.valueOf(conf.getString("frame-item")));
		
		ConfigurationSection keySec = conf.getConfigurationSection("key");
		key = new ItemStack(Material.valueOf(keySec.getString("type")), keySec.getInt("amount"));
		ItemMeta meta = key.getItemMeta();
		if (keySec.isSet("name")) meta.setDisplayName(color(keySec.getString("name")));
		if (keySec.isSet("lore")) meta.setLore(color(keySec.getStringList("lore")));
		key.setItemMeta(meta);
		
		if (conf.isSet("chests")) {
			for (String locStr : conf.getStringList("chests")) chests.add(locStr);
		}
		
		ConfigurationSection lootSec = conf.getConfigurationSection("loot");
		for (String secChance : lootSec.getKeys(false)) {
			ConfigurationSection itemSec = lootSec.getConfigurationSection(secChance);
			String[] args = itemSec.getString("item").split(" ");
			ItemStack stack = new ItemStack(Material.valueOf(args[0]), Integer.parseInt(args[1]));
			ItemMeta stackMeta = stack.getItemMeta();
			stackMeta.setDisplayName(color(args[2]));
			if (itemSec.isSet("lore")) stackMeta.setLore(color(itemSec.getStringList("lore")));
			
			if (itemSec.isSet("enchants")) {
				ConfigurationSection enchSec = itemSec.getConfigurationSection("enchants");
				for (String enchName : enchSec.getKeys(false)) {
					ConfigurationSection enchSubSec = enchSec.getConfigurationSection(enchName);
					stackMeta.addEnchant(Enchantment.getById(enchSubSec.getInt("enchantment")), enchSubSec.getInt("lavel"), true);
				}
			}
			
			stack.setItemMeta(stackMeta);
			loot.put(stack, Float.parseFloat(itemSec.getString("chance")));
		}
	}
}