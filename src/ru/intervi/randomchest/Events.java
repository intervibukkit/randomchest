package ru.intervi.randomchest;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;

public class Events implements Listener {
	public Events(Main main) {
		this.main = main;
	}
	
	private Main main;
	
	public static float getRandomPercent() {
		Random random = new Random();
		return random.nextFloat() + random.nextInt(100);
	}
	
	public ItemStack getRandomLoot() {
		ArrayList<Entry<ItemStack, Float>> list = new ArrayList<Entry<ItemStack, Float>>(main.conf.getLoot());
		Random random = new Random();
		do {
			Entry<ItemStack, Float> entry = list.get(random.nextInt(list.size()));
			if (getRandomPercent() > entry.getValue()) continue;
			return entry.getKey();
		} while(true);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		if (!player.hasPermission("randomchest.use")) return;
		String name = player.getName().toLowerCase();
		main.db.update(name);
		if (main.db.getBlocks(name) < main.conf.blocks) return;
		main.db.resetBlocks(name);
		if (getRandomPercent() > main.conf.keyRandom) return;
		//Location loc = event.getBlock().getLocation();
		//loc.getWorld().dropItemNaturally(loc, main.conf.key);
		player.getInventory().addItem(main.conf.key);
		player.sendMessage(main.conf.keyGive);
	}
	
	private boolean hasEqual(ItemStack item) {
		if (main.conf.key.hasItemMeta() && !item.hasItemMeta()) return false;
		ItemMeta meta = item.getItemMeta();
		ItemMeta keyMeta = main.conf.key.getItemMeta();
		if (keyMeta.hasDisplayName() && (!meta.hasDisplayName() || !meta.getDisplayName().equals(keyMeta.getDisplayName()))) return false;
		if (keyMeta.hasLore() && (!meta.hasLore() || !meta.getLore().equals(keyMeta.getLore()))) return false;
		if (!main.conf.key.getType().equals(item.getType())) return false;
		return true;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (!player.hasPermission("randomchest.use")) return;
		ItemStack item = event.getItem();
		if (item == null || !hasEqual(item) || item.getAmount() < main.conf.itemAmount) return;
		if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || !main.conf.hasChest(event.getClickedBlock().getLocation())) return;
		int newAmount = item.getAmount() - main.conf.itemAmount;
		if (newAmount > 0) item.setAmount(newAmount);
		else {
			item.setAmount(0);
			item.setType(Material.AIR);
		}
		if (main.conf.maxMoney != -1  && main.eco != null) {
			int money = new Random().nextInt(main.conf.maxMoney) + 1;
			EconomyResponse response = main.eco.depositPlayer(player, money);
			if (response.type.equals(ResponseType.SUCCESS))
				player.sendMessage(main.conf.moneyMsg.replaceAll("%money%", String.valueOf(money)));
			else
				main.getLogger().warning("not give " + String.valueOf(response.amount) + " to " + player.getName() + ": " + response.errorMessage);
		}
		Inventory inv = main.getServer().createInventory(null, main.conf.slots, main.conf.inventory);
		int center = (int) (4 + (Math.ceil((main.conf.slots / 9) / 2) * 9));
		int[] frame = {
				(int) (4 + ((Math.ceil((main.conf.slots / 9) / 2) - 1) * 9)),
				(int) (4 + ((Math.ceil((main.conf.slots / 9) / 2) + 1) * 9)),
				center - 1, center + 1
		};
		for (int i = 0; i < main.conf.slots; i++) {
			if (i == center) {
				inv.setItem(i, getRandomLoot());
				continue;
			} else if (i == frame[0] || i == frame[1] || i == frame[2] || i == frame[3]) {
				inv.setItem(i, main.conf.frameItem);
				continue;
			} else inv.setItem(i, main.conf.fillItem);
		}
		player.openInventory(inv);
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		if (!player.hasPermission("randomchest.use") || !event.getInventory().getName().equals(main.conf.inventory)) return;
		if (event.getCurrentItem().equals(main.conf.frameItem) || event.getCurrentItem().equals(main.conf.fillItem))
			event.setCancelled(true);
	}
}