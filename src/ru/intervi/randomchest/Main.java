package ru.intervi.randomchest;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import net.milkbowl.vault.Vault;
import net.milkbowl.vault.economy.Economy;

/**
 * @author InterVi<intervionly@gmail.com>
 */
//по заказу KonondenOF
public class Main extends JavaPlugin implements Listener {
	Config conf = new Config(this);
	DB db = new DB(this);
	Economy eco = null;
	private Events events = new Events(this);
	
	@Override
	public void onEnable() {
		try {
			conf.load();
			db.load();
			db.startTimer();
			if(Bukkit.getPluginManager().getPlugin("Vault") instanceof Vault) {
				RegisteredServiceProvider<Economy> service = Bukkit.getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
				if(service != null) eco = service.getProvider();
				else {
					getLogger().warning("Vault not found");
					return;
				}
			}
			this.getServer().getPluginManager().registerEvents(events, this);
		} catch(Exception e) {e.printStackTrace();}
	}
	
	@Override
	public void onDisable() {
		db.stopTimer();
		db.save();
		db.close();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("randomchest.admin")) {
			sender.sendMessage("нет прав");
			return true;
		}
		if (args == null || args.length == 0) {
			String[] msg = {
				"Нужно указать аргументы:",
				"/rnch set - установить сундук",
				"/rnch remove - удалить сундук",
				"/rnch reload - перезагрузить конфин",
				"/rnch save - сохранить БД"
			};
			sender.sendMessage(msg);
			return true;
		}
		switch(args[0].toLowerCase()) {
		case "set":
			if (!(sender instanceof Player)) {
				sender.sendMessage("эту команду можно использовать только в игре");
				return true;
			}
			Block block = ((Player) sender).getLocation().getBlock();
			if (!block.getType().equals(Material.CHEST) && !block.getType().equals(Material.TRAPPED_CHEST)) {
				sender.sendMessage("это не сундук");
				return true;
			}
			conf.addChestLocation(block.getLocation());
			sender.sendMessage("сундук добавлен");
			break;
		case "remove":
			if (!(sender instanceof Player)) {
				sender.sendMessage("эту команду можно использовать только в игре");
				return true;
			}
			Block block2 = ((Player) sender).getLocation().getBlock();
			if (!block2.getType().equals(Material.CHEST) && !block2.getType().equals(Material.TRAPPED_CHEST)) {
				sender.sendMessage("это не сундук");
				return true;
			}
			if (conf.removeChestLocation(block2.getLocation())) sender.sendMessage("сундук удалён");
			else sender.sendMessage("нет такого сундука");
			break;
		case "reload":
			conf.load();
			sender.sendMessage("конфиг перезагружен");
			break;
		case "save":
			db.save();
			sender.sendMessage("БД сохранена");
			break;
		default:
			return false;
		}
		return true;
	}
}