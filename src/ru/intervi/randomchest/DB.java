package ru.intervi.randomchest;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Timer;
import java.util.TimerTask;

public class DB {
	public DB(Main main) {
		this.main = main;
		PATH = main.getDataFolder().getAbsolutePath() + File.separatorChar + "database.db";
	}
	
	private Main main;
	private Timer timer = null;
	private Connection connection;
	private final String PATH;
	
	private class Saver extends TimerTask {
		@Override
		public void run() {
			save();
		}
	}
	
	private Connection getConnection() {
		try {
			Class.forName("org.sqlite.JDBC");
			return DriverManager.getConnection("jdbc:sqlite:" + PATH);
		}
		catch(SQLException e) {e.printStackTrace(); return null;}
		catch(Exception e) {e.printStackTrace(); return null;}
	}
	
	public void load() {
		try {
			connection = getConnection();
			connection.setAutoCommit(false);
		}
		catch(SQLException e) {e.printStackTrace();}
		catch(Exception e) {e.printStackTrace();}
	}
	
	public void save() {
		try {connection.commit();}
		catch(SQLException e) {e.printStackTrace();}
		catch(Exception e) {e.printStackTrace();}
	}
	
	public void close() {
		try {connection.close();}
		catch(SQLException e) {e.printStackTrace();}
		catch(Exception e) {e.printStackTrace();}
	}
	
	public void startTimer() {
		timer = new Timer();
		timer.schedule(new Saver(), main.conf.saveSec * 1000, main.conf.saveSec * 1000);
	}
	
	public void stopTimer() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}
	
	public int getBlocks(String name) {
		try {
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery("select blocks from blocks where name='" + name + "' limit 1;");
			if (result.next()) return result.getInt("blocks");
			else return -1;
		}
		catch(SQLException e) {e.printStackTrace(); return -1;}
		catch(Exception e) {e.printStackTrace(); return -1;}
	}
	
	public void update(String name) {
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate("create table if not exists blocks (name string, blocks integer);");
			int blocks = getBlocks(name);
			if (blocks != -1)
				statement.executeUpdate("update blocks set blocks=" + String.valueOf(blocks+1) + " where name='" + name + "';");
			else
				statement.executeUpdate("insert into blocks values('" + name + "', 1);");
			//statement.executeUpdate("insert or replace into blocks values('" + name + "', " + (blocks != -1 ? blocks+1 : 1) + ");");
		}
		catch(SQLException e) {e.printStackTrace();}
		catch(Exception e) {e.printStackTrace();}
	}
	
	public void resetBlocks(String name) {
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate("update blocks set blocks=0 where name='" + name + "';");
		}
		catch(SQLException e) {e.printStackTrace();}
		catch(Exception e) {e.printStackTrace();}
	}
}